# README.md

## tl;dr

Run

```sh
docker-commpose up -d
```

## Setup

1. Run `docker-compose up`
    1. `node-red` will try to copy some files and has a permission problem
2. Fix permissions
    2. `sudo chmod -R 777 node-red`
3. Test node-red
    1. Go to [http://localhost:1880](http://localhost:1880)
    2. **MQTT round trip test**
4. Setup InfluxDB
    1. Got to [http://localhost:8086](http://localhost:8086)
    2. Setup InfluxDB with
        1. ORG
        2. BUCKET
        3. TOKEN
5. Setup Telegraf
    1. Modify `telegraf/telegraf.conf` to contain
        1. ORG
        2. BUCKET
        3. TOKEN
6. Setup Grafana
    1. TODO...

Done!

## UIs

Container | URL
-|-
node-red | [http://localhost:1880](http://localhost:1880)
grafana | [http://localhost:3000](http://localhost:3000)
influxdb | [http://localhost:8086](http://localhost:8086)
